/* 
* File: lasernavi.pde
* Author: Jari Tervonen <jjtervonen@gmail.com>
* Description: This is the Arduino firmware for the laser navigation device.
*/

#include <Servo.h>
#include <TimerOne.h>
#include <TimerThree.h>
#include <math.h>

#include <Max3421e.h>
#include <Usb.h>
#include <AndroidAccessory.h>

#define  LASER1         2
#define  LASER2         4

#define  SERVO1         11
#define  SERVO2         10
#define  SERVO3         9
#define  SERVO4         8
 
static long FP_SENSOR = 0xFF88FF88;
static long FP_CONTROL = 0x88FF88FF;
static long FP_SETUP = 0x55FF55FF;

static const byte TURN_BYTE = 0x55;
static const byte POINT_BYTE = 0x88;
static const byte HEADING_BYTE = 0xAA;
static const byte DEMO_MODE = 0xE5;

static const uint8_t DIR_RIGHT = 1;
static const uint8_t DIR_LEFT = 2;
static const uint8_t DIR_FORWARD = 3;
static const uint8_t DIR_AROUND = 4;

AndroidAccessory acc("Uni.Oulu",
		     "LaserNavi",
		     "none",
		     "0.1",
		     "http://www.tol.oulu.fi",
		     "0000000000000001");
Servo servos[4];

static byte msg[52];
static int last_heading = 0;
static int target_heading = 0;
static boolean controlling = false;
static boolean laser_enabled = false;

static int X_SERVO_MIN = 30;
static int X_SERVO_MAX = 150;
static int Y_SERVO_MIN = 30;
static int Y_SERVO_MAX = 140;
static int X_SERVO_CENTER = 90;
static int Y_SERVO_CENTER_CAL = 130;
static int Y_SERVO_CENTER = Y_SERVO_CENTER_CAL;

void init_laser()
{
  digitalWrite(LASER1, LOW);
  digitalWrite(LASER2, LOW);
  pinMode(LASER1, OUTPUT);
  pinMode(LASER2, OUTPUT);
  
}

void setup()
{
  //Serial.begin(115200);
  //Serial.print("HI THERE!\n");
  
  init_laser();
  
  servos[0].attach(SERVO1); //X
  servos[1].attach(SERVO2); //Y
  servos[2].attach(SERVO3); //ROLL1
  servos[3].attach(SERVO4); //ROLL2
  
  center();
  arrowCenter();
  
  acc.powerOn();
}

void enable_point_timer(int seconds)
{
  Timer1.initialize((long)(seconds*1000000));
  Timer1.attachInterrupt( point_timerIsr );
}

void enable_cue_timer(int seconds)
{
  Timer1.initialize((long)(seconds*1000000));
  Timer1.attachInterrupt( cue_timerIsr );
}

void point_timerIsr()
{
    Timer1.stop();
    digitalWrite( LASER1, LOW);
    controlling = false;
}
void cue_timerIsr()
{
    Timer1.stop();
    digitalWrite( LASER2, LOW);
}

void loop()
{
  
  if (acc.isConnected())
  {
    int len = acc.read(msg, 52, 1);
    
    if (len > 0)
    {
      if (checksum(msg) == msg[51])
      {
        if (isSensorPacket())
        {
          if (!controlling)
            handleSensorData();
        }
        else if (isControlPacket())
        {
          handleControlData();
        }
      }
      else
      {
        //XOR NOT OK
        //Serial.println("XOR NOT OK");
      }
    }
  }
  else
  {
    // reset outputs to default values on disconnect
    enableLaser(false, false);
    servos[0].write(X_SERVO_CENTER);
    servos[1].write(Y_SERVO_CENTER_CAL);
    //servos[2].write( (servos[2].read()+10)%180);
    //servos[3].write( (servos[3].read()+10)%180);
    //show_directions();
    //delay(500);
  }
}

void show_directions()
{
  enableLaser(false,true);
  //arrowUp();
  pulse_forward();
  delay(500);
  //arrowLeft();
  pulse_left();
  delay(500);
  //arrowDown();
  pulse_around();
  delay(500);
  //arrowRight();
  pulse_right();
  delay(500);
  enableLaser(false,false);
}

boolean isSensorPacket()
{
  //Serial.println( (long) ( (long)msg[0]<<24 | (long)msg[1]<<16 | (long)msg[2]<<8 | (long)msg[3] ), HEX);
  if ( ( (long)msg[0]<<24 | (long)msg[1]<<16 | (long)msg[2]<<8 | (long)msg[3] ) == FP_SENSOR )
    return true;
  //Serial.print("NOT SENSOR PACKET ");
  return false;
}

boolean isControlPacket()
{
  if ( ((long)msg[0]<<24 | (long)msg[1]<<16 | (long)msg[2]<<8 | (long)msg[3]) == FP_CONTROL )
    return true;

  return false;
}

boolean isSetupPacket()
{
  
  if ( ((long)msg[0]<<24 | (long)msg[1]<<16 | (long)msg[2]<<8 | (long)msg[3]) == FP_SETUP )
    return true;

  return false;
  
}

void handleControlData()
{
  int x = X_SERVO_CENTER;
  int y = Y_SERVO_CENTER;
  
  controlling = msg[4]==0x01 ? true : false;

  if (controlling)
  {
    switch(msg[5])
    {
      case TURN_BYTE:
        controlling = false;
        showTurn((int)msg[6]);
        break;
      case POINT_BYTE:
        x = bytesToInt(msg[6],msg[7]);
        y = bytesToInt(msg[8],msg[9]);
      
        servos[0].write(x);
        servos[1].write(y);
        enableLaser(laser_enabled, false);
        enable_point_timer(5);
        break;
      case HEADING_BYTE:
        target_heading = bytesToInt(msg[6],msg[7]);
        break;
      case DEMO_MODE:
        controlling = false;
        show_directions();
        break;
        
      default:
      break;
    }
  }
  else
    center();
}

void handleSetupData() //TODO: Not tested
{
  uint8_t x_max,y_max,x_min,y_min,x_center,y_center;
  
  x_max = bytesToInt(msg[6],0x00);
  if (isValid(x_max))
    X_SERVO_MAX=x_max;
  
  x_min = bytesToInt(msg[7],0x00);
  if (isValid(x_min))
    X_SERVO_MIN=x_min;
    
  y_max = bytesToInt(msg[8],0x00);
  if (isValid(y_max))
    Y_SERVO_MAX=y_max;
    
  y_min = bytesToInt(msg[9],0x00);
  if (isValid(y_min))
    Y_SERVO_MIN=y_min;
    
  x_center = bytesToInt(msg[10],0x00);
  if (isValid(x_center))
    X_SERVO_CENTER=x_center;
    
  y_center = bytesToInt(msg[11],0x00);
  if (isValid(y_center))
  {
    Y_SERVO_CENTER_CAL=y_center;
    Y_SERVO_CENTER=y_center;
  }
  
}

void handleSensorData()
{
  //check laser enable
  if (msg[5] == 0x0)
  {
    laser_enabled = false;
    enableLaser(false, false);
  }
  else if (msg[5] == 0x1)
  {
    laser_enabled = true;
  }
  
  // Get current heading
  int c_heading = bytesToInt(msg[6], msg[7]);
  last_heading = c_heading;
  // Get ammount of tilt
  int tilt = bytesToInt(msg[8], msg[9]);
  
  //Correct acceleration
  //long x_acc =  (long)msg[10]&0x000000FF | (( (long)msg[11]<<8 ) & 0x0000FF00) | (( (long)msg[12]<<16 ) & 0x00FF0000) | (( (long)msg[13]<<24 ) & 0xFF000000);
  //long y_acc =  (long)msg[14]&0x000000FF | (( (long)msg[15]<<8 ) & 0x0000FF00) | (( (long)msg[16]<<16 ) & 0x00FF0000) | (( (long)msg[17]<<24 ) & 0xFF000000);
  //long z_acc =  (long)msg[16]&0x000000FF | (( (long)msg[17]<<8 ) & 0x0000FF00) | (( (long)msg[18]<<16 ) & 0x00FF0000) | (( (long)msg[19]<<24 ) & 0xFF000000);
  
  //long x_grav =  (long)msg[20]&0x000000FF | (( (long)msg[21]<<8 ) & 0x0000FF00) | (( (long)msg[22]<<16 ) & 0x00FF0000) | (( (long)msg[23]<<24 ) & 0xFF000000);
  //long y_grav =  (long)msg[24]&0x000000FF | (( (long)msg[25]<<8 ) & 0x0000FF00) | (( (long)msg[26]<<16 ) & 0x00FF0000) | (( (long)msg[27]<<24 ) & 0xFF000000);
  //long z_grav =  (long)msg[28]&0x000000FF | (( (long)msg[29]<<8 ) & 0x0000FF00) | (( (long)msg[30]<<16 ) & 0x00FF0000) | (( (long)msg[31]<<24 ) & 0xFF000000);
      
  //Serial.print("acceleration X:");Serial.println(float((float)x_acc/1000000.0f));
  //Serial.print("acceleration Y:");Serial.println(float(y_acc/1000000));
  //Serial.print("acceleration Z:");Serial.println(float(z_acc/1000000));
        
  //Serial.print("gravitation X:");Serial.println(float(x_grav/1000000));
  //Serial.print("gravitation Y:");Serial.println(float(y_grav/1000000));
  //Serial.print("gravitation Z:");Serial.println(float(z_grav/1000000));
        
  //double t = atan2( (double)z_grav/1000000.0 , (double)y_grav/1000000.0 );
  
  //Serial.println(tilt);
  if (tilt<=90 && tilt>=-30)
  {
    Y_SERVO_CENTER = Y_SERVO_CENTER_CAL-tilt;
    //Y_SERVO_CENTER = Y_SERVO_CENTER_CAL+map(tilt, 0, 180, Y_SERVO_MIN, Y_SERVO_MAX);
    servos[1].write(Y_SERVO_CENTER);
  }
}

void enableLaser(boolean one, boolean two)
{
  if (one)
    digitalWrite(LASER1, HIGH);
  else
    digitalWrite(LASER1, LOW);
    
  if (two)
    digitalWrite(LASER2, HIGH);
  else
    digitalWrite(LASER2, LOW);
}

void center()
{
  servos[0].write(X_SERVO_CENTER);
  servos[1].write(Y_SERVO_CENTER);
  arrowCenter();
}

void showTurn(uint8_t dir)
{
  switch (dir)
  {
    case DIR_RIGHT:
      pulse_right();
    break;
    case DIR_LEFT:
      pulse_left();
    break;
    case DIR_FORWARD:
      pulse_forward();
    break;
    case DIR_AROUND:
      pulse_around();
    break;
    default:
    break;
  }
}

void arrowDown()
{
  servos[2].write(180);
  servos[3].write(180);
}

void arrowUp()
{
  servos[2].write(85);
  servos[3].write(90);
}

void arrowLeft()
{
  servos[2].write(130);
  servos[3].write(130);
}

void arrowRight()
{
  servos[2].write(45);
  servos[3].write(45);
}

void arrowCenter()
{
  servos[2].write(85);
  servos[3].write(90);
}

void pulse_around()
{
  uint8_t times = 0;
  uint8_t i;
  uint8_t move_ammount = 30;
  
  uint8_t startpy = servos[1].read();
  
  if (startpy+move_ammount > Y_SERVO_MAX)
    move_ammount = Y_SERVO_MAX-startpy;
  
  center();
  arrowDown();
  delay(50);
  
  for(;times<2;times++)
  {
    enableLaser(false,laser_enabled);
    for (i=startpy;i<startpy+move_ammount;i++)
    {
      servos[1].write(i);
      delay(10);
    }
    enableLaser(false,false);
    servos[1].write(startpy);
    delay(250);
  }
  enableLaser(false, laser_enabled);
  delay(1000);
  center();
  enableLaser(false,false);
}

void pulse_forward()
{
  uint8_t times = 0;
  uint8_t i;
  uint8_t move_ammount = 30;
  
  uint8_t startpy = servos[1].read();
  
  if (startpy-move_ammount < Y_SERVO_MIN)
    move_ammount = startpy - Y_SERVO_MIN;
  
  center();
  arrowUp();
  
  for(;times<2;times++)
  {
    enableLaser(false, laser_enabled);
    for (i=startpy;i>startpy-move_ammount;i--)
    {
      servos[1].write(i);
      delay(10);
    }
    enableLaser(false,false);
    servos[1].write(startpy);
    delay(250);
  }
  enableLaser(false, laser_enabled);
  delay(1000);
  enableLaser(false,false);
  center();
}

void pulse_left()
{
  uint8_t times = 0;
  uint8_t i;
  uint8_t startpx = servos[0].read();
  
  center();
  arrowLeft();
  
  for(;times<2;times++)
  {
    enableLaser(false, laser_enabled);
    for (i=startpx;i<startpx-30;i--)
    {
      servos[0].write(i);
      delay(10);
    }
    enableLaser(false,false);
    servos[0].write(startpx);
    delay(250);
  }
  enableLaser(false, laser_enabled);
  delay(1000);
  enableLaser(false,false);
  center();
}

void pulse_right()
{
  uint8_t times = 0;
  uint8_t i;
  uint8_t startpx = servos[0].read();
  
  center();
  arrowRight();
  
  for(;times<2;times++)
  {
    enableLaser(false, laser_enabled);
    for (i=startpx;i>startpx+30;i++)
    {
      servos[0].write(i);
      delay(10);
    }
    enableLaser(false,false);
    servos[0].write(startpx);
    delay(250);
  }
  enableLaser(false, laser_enabled);
  delay(1000);
  enableLaser(false,false);
  center();
}

/*
* Check that the values stay in 0:180 range which are the physical limits of servos.
*/
boolean isValid(int val)
{
  if (val <= 180 && val>=0)
    return true;
  return false;
}

/*
* Merge two bytes to one integer.
*/
inline static int bytesToInt(byte msb, byte lsb)
{
  //I trust myself not to feed this function with bad parameters
  int ret = (int)msb | ( ( (int)lsb << 8 ) & 0xFF00 );
  return ret;
}

/*
* Basic XOR checksum calculation.
*/
static uint8_t checksum(byte b [])
{
  int XOR = 0;
  for (int i=0;i<50;i++)
  {
    XOR ^= b[i];
  }
  return XOR;
}
